import React, { useState, useEffect, useCallback, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
// import { MovieSearch } from '../movie-grid/MovieGrid';
import Button, { OutlineButton } from "../button/Button";
import Input from "../input/Input";
import { category } from "../../api/tmdbApi";

import "./header.scss";

    // const headerNav = [

    // //   {
    // //     display: "Home",
    // //     path: "/",
    // //   },
    // //   {
    // //     display: "Movies",
    // //     path: "/movie",
    // //   },
    // //   {
    // //     display: "TV Series",
    // //     path: "/tv",
    // //   },
    // ];

export const MovieSearch = (props) => {
    const navigate = useNavigate();

    const [keyword, setKeyword] = useState(props.keyword ? props.keyword : "");

    const goToSearch = useCallback(() => {
        if (keyword.trim().length > 0) {
        navigate(`/${category.movie}/search/${keyword}`);
        }
        if (keyword.trim().length > 0) {
        navigate(`/${category.tv}/search/${keyword}`);
        }
    }, [keyword, navigate]);

    useEffect(() => {
        const enterEvent = (e) => { e.preventDefault();
        if (e.keyCode === 13) {
            goToSearch();
        }
        };
        
        document.addEventListener("keyup", enterEvent);
        return () => {
        document.removeEventListener("keyup", enterEvent);
        };

    }, [keyword, goToSearch]);

    return (
        <div className="movie-search">
        <Input
            type="text"
            placeholder="Enter keyword"
            value={keyword}
            onChange={(e) => setKeyword(e.target.value)}
        />
        <Button className="small" onClick={goToSearch}>
            Search
        </Button>
        </div>
        
    );
    };

const Header = () => {
    //   const { pathname } = useLocation();
    const headerRef = useRef(null);

    //   const active = headerNav.findIndex(e => e.path === pathname);

    useEffect(() => {
        const shrinkHeader = () => {
        if (
            document.body.scrollTop > 100 ||
            document.documentElement.scrollTop > 100
        ) {
            headerRef.current.classList.add("shrink");
        } else {
            headerRef.current.classList.remove("shrink");
        }
        };
        window.addEventListener("scroll", shrinkHeader);
        return () => {
        window.removeEventListener("scroll", shrinkHeader);
        };
    }, []);

    return (
        <div ref={headerRef} className="header">
        <div className="header__wrap container">
            <div className="logo">
            <Link to="/">MovieList</Link>
            </div>

            <MovieSearch />

            <ul className="header__nav">
            <Button className="small">Login</Button>
            <OutlineButton className="small">Register</OutlineButton>

            {/* {
                    headerNav.map((e, i) => (
                        <li key={i} className={`${i === active ? 'active' : ''}`}>
                            <Link to={e.path}>
                                {e.display}
                            </Link>
                        </li>
                    ))
                } */}
            </ul>
        </div>
        </div>
    );
};

export default Header;
